﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    [SerializeField] private Animator _animator = null;
    
    
    //private int _runID = Animator.StringToHash("IsRun");


    public void SetTrigger(string name) //можно передавать в качестве аргумента инт _runId , его задаем как на примере выше
    {

        _animator.SetTrigger(name);
    }
    public void ResetTrigger(string name) //можно передавать в качестве аргумента инт _runId , его задаем как на примере выше
    {

        _animator.ResetTrigger(name);
    }
}
