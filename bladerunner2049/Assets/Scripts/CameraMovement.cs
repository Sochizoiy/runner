﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Player _player = null;
    private void Update()
    {
        Move();
    }
    private void Move()
    {
        Vector3 forwardOffset = _player.PlayerSpeed * Time.deltaTime * Vector3.forward;
        transform.Translate(forwardOffset);
    }
}
