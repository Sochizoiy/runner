﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Config", menuName = "Config/MainConfig")]
public class Config : ScriptableObject
{
    public float Speed = 0f;

}

