﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    private float _startPosition;
    [HideInInspector] public float _offset;
    public float Offset => _offset; //св-во чтобы снаружи нельзя было менять, только получать
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _startPosition = Input.mousePosition.x;
        }

        if (Input.GetMouseButton(0))
        {
            _offset = -(_startPosition - Input.mousePosition.x);
            _offset /= Screen.width; // от 0 до 1 будет, и не важно размер экрана
            _startPosition = Input.mousePosition.x;
        }
        if (Input.GetMouseButtonUp(0))
        {
            _offset = 0f;
        }
    }
}