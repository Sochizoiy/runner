﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    [SerializeField] private GameObject _roadPrefab = null;
    [SerializeField] private Transform _startPoint = null;
    [SerializeField] private int _startSegmentCount = 10;

    private Vector3 _lastPoint;

    private void Start()
    {
        GenerateRoad();
    }

    public void GenerateRoad()
    {
        Vector3 generationPosition = _startPoint.position;
        for (int i = 0; i < _startSegmentCount; i++)
        {
            GameObject newPlatform = Instantiate(_roadPrefab);
            newPlatform.transform.position = generationPosition;
            newPlatform.transform.SetParent(transform); //делаем лвл родителем кусочков дороги
            generationPosition = newPlatform.GetComponentInChildren<RoadSegment>().NextPosition;

            newPlatform.GetComponentInChildren<RoadSegment>().OnInvisible += SegmentReplacer;

            if (i == _startSegmentCount - 1 )
            {
                _lastPoint = generationPosition;
            }
        }

    }

    private void SegmentReplacer (RoadSegment roadSegment)
    {
        roadSegment.ParentPosition = _lastPoint;
        _lastPoint = roadSegment.NextPosition;
    }

}