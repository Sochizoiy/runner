﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{
    [SerializeField] private float _roadWidth = 1f;
    [SerializeField] private float _speed = 0f;
    [SerializeField] private int _lifeCount = 3;
    [SerializeField] private Camera _camera = null;
    [SerializeField] private InputHandler _inputHandler = null;
    [SerializeField] private AnimationController _animationController = null;
    [SerializeField] private int _coinsToGetLife = 50;
    [SerializeField] private TextMeshProUGUI _coinsCounter = null;
    [SerializeField] private TextMeshProUGUI _lifesCounter = null;
    [SerializeField] private GameObject _gameScreen = null;
    [SerializeField] private GameObject _restartScreen = null;
    [SerializeField] private SaveSystem SaveSystem = null;
    private Coroutine _coroutine = null;
    private Vector3 _playerStartPos;
    bool isDance = false;
    int _startCoins = 0;
    [SerializeField] private int _coins;

    public float PlayerSpeed
    {
        get
        {
            return _speed;
        }
        set
        {
            if (value >= 0)
            {
                _speed = value;
            }
        }
    }
    public int PlayerCoins
    {
        get
        {
            return _coins;
        }
        set
        {
            if (value >= 0)
            {
                _coins = value;
            }
        }
    }

    public int PlayerLifes
    {
        get
        {
            return _lifeCount;
        }
        set
        {
            if (value >= 0)
            {
                _lifeCount = value;
            }
        }
    }
    public void Start()
    {

        PlayerCoins = SaveSystem.gameData.Coins;
        PlayerLifes = SaveSystem.gameData.Health;
        PlayerSpeed = 0;
        _playerStartPos = transform.position;
    }

    private void Update()
    {
        Move();

        if (_inputHandler.Offset > 0 && transform.position.z == _playerStartPos.z)
        {
            PlayerSpeed = 12f;
            _animationController.SetTrigger("IsRun");
            _animationController.ResetTrigger("StandUp");
        }
        if (PlayerCoins == _coinsToGetLife && isDance == false)
        {
            _coroutine = StartCoroutine(Win());
        }
        _coinsCounter.text = "Coins: " + PlayerCoins;
        _lifesCounter.text = "HP: " + PlayerLifes;
    }

    private void Move()
    {
        Vector3 horizontalOffset = _inputHandler.Offset * _roadWidth * Vector3.right;

        float resultOffset = transform.position.x + horizontalOffset.x; //работает если центр уровня в нуле, а персонаж спавнится в границах дороги.
        if (Mathf.Abs(resultOffset) > _roadWidth / 2f) //Модуль математический
        {
            horizontalOffset.x = 0f;
        }
        if (PlayerSpeed == 0f )
        {
            horizontalOffset.x = 0f;
        }

        Vector3 forwardOffset = _speed * Time.deltaTime * Vector3.forward;
        transform.Translate(forwardOffset + horizontalOffset);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            _coroutine = StartCoroutine(Die());
        }
        if(other.gameObject.tag == "Coin")
        {
            Destroy(other.gameObject);
            PlayerCoins += 1;
        }

    }

    private IEnumerator Die()
    {
        _speed = 0f;
        PlayerLifes -= 1;
        _animationController.ResetTrigger("IsRun");
        _animationController.SetTrigger("IsDead");
        yield return new WaitForSeconds(3f);
        yield return new WaitForSeconds(3f);
        DestroyAllOnScene();
        _animationController.ResetTrigger("IsDead");
        _animationController.SetTrigger("StandUp");
        _coroutine = null;
        if (PlayerLifes == 0)
        {
            gameObject.SetActive(false);
            _gameScreen.SetActive(false);
            _restartScreen.SetActive(true);
            PlayerLifes = 3;
            PlayerCoins = 0;
            //Сюда поместить возможность игроку за просмотр реварда получить еще одну жизнь. Если откажется, экран рестарта.
        }
        _playerStartPos = transform.position;
    }
    private IEnumerator Win()
    {
        _speed = 0f;
        PlayerCoins = 0;
        isDance = true;
        _animationController.ResetTrigger("IsRun");
        _animationController.SetTrigger("IsDance");
        yield return new WaitForSeconds(3f);
        DestroyAllOnScene();
        _animationController.ResetTrigger("IsDance");
        _animationController.SetTrigger("StandUp");
        isDance = false;
        _coroutine = null;
        _playerStartPos = transform.position;
        PlayerLifes += 1;
    }

    private void DestroyAllOnScene()
    {
        var vragi = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (var i in vragi)
        {
            Object.Destroy(i);
        }
        var coinsOnScene = GameObject.FindGameObjectsWithTag("Coin");
        foreach (var i in coinsOnScene)
        {
            Object.Destroy(i);
        }
    }

}