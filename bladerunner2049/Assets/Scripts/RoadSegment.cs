﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadSegment : MonoBehaviour
{
    [SerializeField] private Transform _nextPoint = null;
    [SerializeField] private Transform _parent = null;
    public Vector3 ParentPosition
    {
        get
        {
            return _parent.position;
        }
        set
        {
            _parent.position = value;
        }
    }
    public Vector3 NextPosition => _nextPoint.position;

    public System.Action<RoadSegment> OnInvisible = null;

    private void OnBecameInvisible()
    {
        OnInvisible?.Invoke(this);
    }

    
}

 
