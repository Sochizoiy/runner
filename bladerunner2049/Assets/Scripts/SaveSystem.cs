﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveSystem : MonoBehaviour
{
    [SerializeField] private Player _player = null;

   public  GameData gameData = new GameData();

    private void Awake()
    {
        if (PlayerPrefs.HasKey("GameData"))
        {
            gameData = JsonUtility.FromJson<GameData>(PlayerPrefs.GetString("GameData"));

        }
    }
    private void OnApplicationQuit()
    {
        gameData.Health = _player.PlayerLifes;
        gameData.Coins = _player.PlayerCoins;
        string json = JsonUtility.ToJson(gameData, true);
        PlayerPrefs.SetString("GameData", json);
    }
}
[System.Serializable]
public class GameData
{
    public int Coins;
    public int Health;
    public GameData()
    {
        Coins = 0;
        Health = 3;
    }

}
