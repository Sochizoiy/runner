﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject _prefabObstacle = null;
    [SerializeField] private GameObject _prefabCoin = null;
    [SerializeField] private Player _player = null;
    [SerializeField] private int _nomerSpawnera = 0;

    private Coroutine _coroutine = null;


    private void Start()
    {
        _coroutine = StartCoroutine(SpawnController());
    }

    private IEnumerator SpawnController()
    {
        for (int i = 0; i < 5001; i++)
        {
            if (_player.PlayerSpeed != 0f)
            {
                int a = ((i + _nomerSpawnera) * (i + 15) + (i * i * _nomerSpawnera));
                float b = a % 10f;
                yield return new WaitForSeconds(b + 1f);
                SpawnObstacle();
                yield return new WaitForSeconds(Random.Range(1f, 2f));
                SpawnCoin();
                if (i == 4999)
                {
                    i = 0;
                }
            }
            else
            {
                yield return new WaitForSeconds(0.5f);
                if (i == 4999)
                {
                    i = 0;
                }
            }
        }
    }

    public void SpawnObstacle()
    {
        GameObject Obstacle = Instantiate(_prefabObstacle, transform.position, transform.rotation, null);
    }
    public void SpawnCoin()
    {
        GameObject Obstacle = Instantiate(_prefabCoin, transform.position,Quaternion.identity ,null);
    }

}